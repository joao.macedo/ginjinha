#!/usr/bin/env python
#-*- coding:utf-8 -*-

from setuptools import setup

setup(
    name = "ginjinha",
    version = "1.0.0",
    description = "Load a yaml file format and apply "
        "dynamic render with jinja2 templates.",
    long_description = "Renders jinja2 templates on yaml settings using it "
        "self as a context to be rendered."
        "The template keywords can use full path refering to any settings "
        "object or any keyword of its own level dictionary. "
        "If variables are referenced before its asignement, in the template, "
        "they will remain raw (unrendered). This capability can be used for "
        "defining general commands which will have different renderization "
        "depending of the context.",
    author = "Joao Macedo",
    author_email = "magau.macedo@gmail.com",
    license = "Apache license",
    url = "https://gitlab.com/joao.macedo/ginjinha",
    packages = ['ginjinha'],
    package_dir={"": "src"},
    install_requires = ["pyyaml", "jinja2"],
)
