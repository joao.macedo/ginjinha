#!/usr/bin/env python
#-*- coding:utf-8 -*

"""Renders jinja2 templates on yaml file format using it self
as the context to be rendered.
The template keywords can use full path refering to any
object or any keyword of its own level dictionary.
If variables are referenced before its asignement, in the template,
they will remain raw (unrendered).
"""
import os
from jinja2 import Environment, meta
import yaml
import logging
from itertools import product
from datetime import timedelta
from functools import reduce


logger = logging.getLogger(__name__)

def load_settings(in_path, context={}, env=None):
    """Loads a YAML file and returns its rendered contents
    with jinja2 templates.
    @param filename - Path to the YAML file.
    @param, optional, context - Additional context to be used
                                in the template renderization.
    @retval settings - Loaded settings.
    """
    raw_settings = {}
    if os.path.isdir(in_path):
        for root, dirs, files in os.walk(in_path):
            for path in [os.path.join(root, fname)
                         for fname in files if fname.endswith('.yml')]:
                raw_settings.update(_load_raw_from_file(path))
    elif os.path.isfile(in_path): 
        raw_settings = _load_raw_from_file(in_path)

    if any(raw_settings):
        settings = dict(raw_settings)
        _render_settings(settings, context, env=env)
        return settings


def _load_raw_from_file(filename):
    try:
        with open(filename, "r") as yaml_fd: 
            raw_settings = yaml.safe_load(yaml_fd)
        return raw_settings
    except:
        logger.error("Can't open YAML file: "
                     "{}".format(filename))
        return {}


def _render_settings(item, context={}, parent=None,
                     access_key=None, env=None):

    if env is None:
        env = Environment()
    _add_custom_filters(env)

    if (isinstance(item, str) or isinstance(item, bytes)):
       item = _render_string(item, context, env)
       parent[access_key] = item

    if isinstance(item, list):
        _render_list(item, context, env)

    elif isinstance(item, dict):
        _render_dict(item, context, env)


def _add_custom_filters(env):

    env.filters['format'] = lambda val, fmt_str:(
        format(val, fmt_str)
    )

    env.filters['product'] = lambda vals, format_str:( 
        [format_str.format(**unique_pairs) for unique_pairs in
        [dict(zip(vals.keys(), combi))
         for combi in product(*vals.values())]]
    )


def _render_string(item, context, env):
    template = env.from_string(item)
    undeclared_vars = meta.find_undeclared_variables(env.parse(item))
    check_context = [var in context.keys() for var in undeclared_vars]
    if len(check_context) != 0 and all(check_context):
        item = yaml.safe_load(template.render(context))
    elif len(check_context) == 0:
        item = template.render(context)
    return item

def _render_list(item, context, env):
    for idx, element in enumerate(item):
        if isinstance(element, dict):
            new_context = dict(context)
            new_context.update(element)
            _render_settings(element, new_context, item, idx, env)
        else:
            _render_settings(element, context, item, idx, env)


def _render_dict(item, context, env):
    if not any(context):
        context = item
    for key, value in item.items():
        new_context = dict(context)
        new_context.update(item)
        _render_settings(value, new_context, item, key, env)


def get_rendered(settings, context, path_list=None, env=None):

    if env is None:
        env = Environment()
    _add_custom_filters(env)

    rendered = dict(settings)
    _render_settings(rendered, context, env=env)
    if path_list is not None:
        return reduce_dict(rendered, path_list)
    else:
        return rendered


def reduce_dict(dict_tree, kw_list, default=None):
    """Get value from a dictionary of dictionaries.
    A list of keywords must be given pointing to the
    respective value's path. 
    """
    return reduce(lambda d, k: d.get(k) if isinstance(d, dict)
                  else default, kw_list, dict_tree)

